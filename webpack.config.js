const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
var webpack = require('webpack');
module.exports = {
    entry: __dirname + '/src/app/app.js',
    output: {
        path: __dirname + '/dist',
        filename: 'bundle.js',
        publicPath: './'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                use: 'babel-loader',
                exclude: [
                    /node_modules/
                ]
            },
            {
                test: /\.s?css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'postcss-loader',
                    'sass-loader'
                ]
            },
            {
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'file-loader',
                options: {
                    name: 'fonts/[name].[ext]'
                }
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: __dirname + '/src/public/index.html',
            filename: 'index.html'
        }),
        new HtmlWebpackPlugin({
            template: __dirname + '/src/public/projects.html',
            filename: 'projects.html'
        }),
        new HtmlWebpackPlugin({
            template: __dirname + '/src/public/one-project.html',
            filename: 'one-project.html'
        }),
        new HtmlWebpackPlugin({
            template: __dirname + '/src/public/services.html',
            filename: 'services.html'
        }),
        new HtmlWebpackPlugin({
            template: __dirname + '/src/public/about.html',
            filename: 'about.html'
        }),
        new HtmlWebpackPlugin({
            template: __dirname + '/src/public/404.html',
            filename: '404.html'
        }),
        new CopyPlugin({
            patterns: [
                { from: 'src/public/images', to: 'images' },
            ],
        }),
        new CopyPlugin({
                patterns: [
                    { from: 'src/public/favicon.svg', to: './' },
                    { from: 'src/public/logo.svg', to: './' },
                ],
            }),
        new MiniCssExtractPlugin({
            filename: 'bundle.css'
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery'
        })
    ],
    devServer: {
        contentBase: './src/public',
        port: 7700,
    }
}