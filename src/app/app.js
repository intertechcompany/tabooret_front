// CSS
import '../style/slick.css';
import '../style/reset.css';
import '../style/grid.scss';
import '../style/app.scss';
import '../style/plyr/plyr.scss';

// JS
import $ from 'jquery';
global.jQuery = $;
global.$ = $;
import '../app/slick.min.js';
import '../app/common.js';
