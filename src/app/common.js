/* Lazy */

$(".animated").show();

const images = document.querySelectorAll(".lazy-img");
const blocks = document.querySelectorAll(".video");

const options = {
  root: null,
  rootMargin: "0px",
  treshold: 0.1,
};

function handleImg(myImg, observer) {
  myImg.forEach((myImgSingle) => {
    loadImage(myImgSingle.target);
    setTimeout(function () {
      projectBottom();
    }, 100);
    if (myImgSingle.intersectionRatio > 0) {
      opacityImage(myImgSingle.target);
      setTimeout(function () {
        multiLineTitle();
      }, 500);
    }
  });
}

function handleBlock(myBlock, observer) {
  myBlock.forEach((myBlockSingle) => {
    if (myBlockSingle.intersectionRatio > 0) {
      opacityBlock(myBlockSingle.target);
    }
  });
}

function opacityBlock(block) {
  $(block).parents(".lazy").addClass("loaded");
}

const observerVideo = new IntersectionObserver(handleBlock, options);

blocks.forEach((block) => {
  observerVideo.observe(block);
});

$(window).resize(function () {
  projectBottom();
});

function loadImage(image) {
  image.src = image.getAttribute("data-src");
}

function opacityImage(image) {
  // image.classList.add('loaded');
  $(image).parents(".lazy").addClass("loaded");
}

function projectBottom() {
  $(".project-item-bottom").each(function () {
    let imgWidth = $(this).siblings(".tag-wrapper").width();

    $(this).css("max-width", imgWidth);
  });
}

const observer = new IntersectionObserver(handleImg, options);

images.forEach((img) => {
  observer.observe(img);
});

/* Multi-Line */

setTimeout(function () {
  multiLineLink();
}, 500);

$(window).resize(function () {
  multiLineLink();
  multiLineTitle();
});

function multiLineTitle() {
  $(".prject-item-link .block-title").each(function () {
    let $el = $(this),
      s = $el.text().split(/\s+/),
      h = { curr: 0, prev: 0 },
      parts = [],
      word,
      back = [];

    while (s.length) {
      word = s.shift();
      back.push(word);
      $el.html(back.join(" "));
      h.curr = $el.height();

      if (h.curr !== h.prev) {
        parts.push([]);
        h.prev = h.curr;
      }

      parts[parts.length - 1].push(word);
    }

    parts = parts.map((e, i) => "<span>" + e.join(" ") + "</span>");

    $el.html(parts.join("\n"));
  });
}

function multiLineLink() {
  $(".multi-line>span").each(function () {
    let $el = $(this),
      s = $el.text().split(/\s+/),
      h = { curr: 0, prev: 0 },
      parts = [],
      word,
      back = [];

    while (s.length) {
      word = s.shift();
      back.push(word);
      $el.html(back.join(" "));
      h.curr = $el.height();

      if (h.curr !== h.prev) {
        parts.push([]);
        h.prev = h.curr;
      }

      parts[parts.length - 1].push(word);
    }

    parts = parts.map((e, i) => "<span>" + e.join(" ") + "</span>");

    $el.html(parts.join("\n"));
  });
}

/* Menu Toggle */

$("#menu-toggle").click(function () {
  $(this).toggleClass("active");
  $(".menu").toggleClass("active");
});

$(document).mouseup(function (e) {
  let div = $(".sticky-header");
  if ($(window).width() < 992 && !div.is(e.target) && div.has(e.target).length === 0) {
    $("#menu-toggle").removeClass("active");
    $(".menu").removeClass("active");
  }
});

/* Animated Anchor Scroll */

let $page = $("html, body");

$('a[href*="#"]').click(function () {
  $page.animate(
    {
      scrollTop: $($.attr(this, "href")).offset().top,
    },
    1000
  );
  return false;
});

var myHash = location.hash;

if (myHash[1] != undefined) {
  $("html, body").animate({ scrollTop: $(myHash).offset().top - 40 }, 1000);
}

/* To Bottom Animated Scroll */

if ($("#to-bottom").length) {
  toBottomToggle();

  $(window).bind("resize scroll", function () {
    toBottomToggle();
  });

  $("#to-bottom").click(function (e) {
    let anchor = $(this);

    $("html, body")
      .stop()
      .animate(
        {
          scrollTop: $(anchor.attr("href")).offset().top - 85,
        },
        1000
      );
    e.preventDefault();

    return false;
  });
}

function toBottomToggle() {
  if ($(window).width() < 992) {
    $("#to-bottom").hide();
  } else {
    let topOffset = $("#to-bottom-anchor").offset().top - 85;

    if ($(window).scrollTop() >= topOffset) {
      $("#to-bottom").fadeOut();
    } else {
      $("#to-bottom").fadeIn();
    }
  }
}

/* Header Scroll */

let header = $(".sticky-header"),
  scrollPrev = 0;

$(window).scroll(function () {
  let scrolled = $(window).scrollTop();

  if (scrolled > 100 && scrolled > scrollPrev) {
    header.addClass("out");
    $(".menu, #menu-toggle").removeClass("active");
  } else {
    header.removeClass("out");
  }

  scrollPrev = scrolled;
});

/* Writing Animation */

writing();

$(".animate-start").mouseenter(function () {
  let path = $(this).find(".animated path");

  path.css({
    transition: "stroke-dashoffset 0.3s ease-in-out",
    "stroke-dashoffset": "0",
  });
});

$(".animate-start").mouseleave(function () {
  let path = document.querySelectorAll("a:not(.service-toggler) .animated path");

  for (let el of path) {
    length = el.getTotalLength();
    el.style.strokeDasharray = length + " " + length;
    el.style.strokeDashoffset = length;
    el.getBoundingClientRect();
  }
});

function writing() {
  let path = document.querySelectorAll(".animated path");

  for (let el of path) {
    length = el.getTotalLength();
    el.style.strokeDasharray = length + " " + length;
    el.style.strokeDashoffset = length;
    el.getBoundingClientRect();
  }

  setTimeout(function () {
    let animateWidth = $(".animate-4").parent().width();

    $(".animate-4").attr({
      width: animateWidth,
      viewBox: "0 0 " + animateWidth + " 8",
    });
  }, 300);
}

/* Main Page Service Tabs */

firstBlock();

// $(window).resize(function() {
//     firstBlock();
// });

function firstBlock() {
  let first = $(".service-trigger-list .service-trigger-item:first-child");

  first.find(".service-toggler .animated path").css({
    transition: "stroke-dashoffset 0.3s ease-in-out",
    "stroke-dashoffset": "0",
  });
  $(".service-desc").hide();
  first.children(".service-desc").show();
}

$(".service-toggler").click(function () {
  if ($(window).width() < 992) {
    $(".service-desc").hide();
    $(this).siblings(".service-desc").show();

    let path = document.querySelectorAll(".service-toggler .animated path");

    for (let el of path) {
      length = el.getTotalLength();
      el.style.strokeDasharray = length + " " + length;
      el.style.strokeDashoffset = length;
      el.getBoundingClientRect();
    }

    let path2 = $(this).find(".animated path");

    path2.css({
      transition: "stroke-dashoffset 0.3s ease-in-out",
      "stroke-dashoffset": "0",
    });
  }
});

$("a.service-toggler").mouseenter(function () {
  if ($(window).width() > 991) {
    $(".service-desc").hide();
    $(this).siblings(".service-desc").show();

    let path = document.querySelectorAll(".service-toggler .animated path");

    for (let el of path) {
      length = el.getTotalLength();
      el.style.strokeDasharray = length + " " + length;
      el.style.strokeDashoffset = length;
      el.getBoundingClientRect();
    }

    let path2 = $(this).find(".animated path");

    path2.css({
      transition: "stroke-dashoffset 0.3s ease-in-out",
      "stroke-dashoffset": "0",
    });
  }
});

/* Horizontal Scroller */

if ($(".main-content").hasClass("about-us-content")) {
  let element = document.querySelector(".scroller-wrapper");

  let Visible = function (target) {
    let scrollerPart = $(".scroller-wrapper").height() * 0.75;

    let targetPosition = {
        top: window.pageYOffset + target.getBoundingClientRect().top + scrollerPart,
        bottom: window.pageYOffset + target.getBoundingClientRect().bottom - scrollerPart,
      },
      windowPosition = {
        top: window.pageYOffset,
        bottom: window.pageYOffset + document.documentElement.clientHeight,
      };

    let path = $(window).height() - $(".scroller-wrapper").height() / 2,
      limit = -($(".scroller").width() - $(window).width()) + 10;

    if (targetPosition.bottom > windowPosition.top && targetPosition.top < windowPosition.bottom) {
      let percent = ((windowPosition.bottom - targetPosition.top) * 100) / path,
        tx = (limit * percent) / 100;

      $(".scroller").css("transform", "matrix(1, 0, 0, 1, " + tx + ", 0)");
    } else {
      if (targetPosition.bottom < windowPosition.top) {
        $(".scroller").css("transform", "matrix(1, 0, 0, 1, " + limit + ", 0)");
      }

      if (targetPosition.top > windowPosition.bottom) {
        $(".scroller").css("transform", "matrix(1, 0, 0, 1, 0, 0)");
      }
    }
  };

  function scrollerSize() {
    let scrollerWidth = 0,
      scrollerHeight = 0;

    scrollerHeight = $(".scroller-content").height();

    $(".scroller-wrapper").height(scrollerHeight);

    $(".scroller-content").each(function () {
      scrollerWidth += $(this).width() + 20;
    });

    scrollerWidth = scrollerWidth - 20;

    $(".scroller").width(scrollerWidth);
  }

  setTimeout(function () {
    scrollerSize();
  }, 100);

  setTimeout(function () {
    Visible(element);
  }, 400);

  $(window).resize(function () {
    setTimeout(function () {
      scrollerSize();
    }, 100);

    setTimeout(function () {
      Visible(element);
    }, 400);
  });

  $(window).scroll(function () {
    Visible(element);
  });
}

/* Slick */

$(".slick-slider").slick({
  lazyLoad: "ondemand",
  slidesToShow: 1,
  slidesToScroll: 1,
  prevArrow: `<button class='slick-prev'><svg width='75' height='62' viewBox='0 0 75 62' fill='none' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' clip-rule='evenodd' d='M32.6562 1.80604L30.8941 -0.000167327L6.06669e-05 30.8938L30.8777 61.7715L32.6692 59.8905L5.08161 32.3029L74.45 32.3029L74.45 29.4847L5.09528 29.4847L32.6562 1.80604Z' fill='#212121' /></svg></button>`,
  nextArrow: `<button class='slick-next'><svg width="75" height="62" viewBox="0 0 75 62" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M41.7937 1.80604L43.5559 -0.000167327L74.4499 30.8938L43.5722 61.7715L41.7808 59.8905L69.3683 32.3029L3.51409e-07 32.3029L5.082e-07 29.4847L69.3547 29.4847L41.7937 1.80604Z" fill="#212121"/></svg></button>`,
});

/* Plur */

import Plyr from "plyr";

if ($(".video-main").length) {
  const player = new Plyr(".video-main", {
    muted: true,
    controls: ['play-large', 'play', 'progress', 'current-time', 'mute', 'volume', 'captions', 'pip', 'airplay', 'fullscreen']
  });
  player.on("ready", () => {
    player.play();
  });
}

if ($(".video-secondary").length) {
  const players = Array.from(document.querySelectorAll(".video-secondary")).map(
    (p) =>
      new Plyr(p, {
        controls: ["play-large", "play", "progress", "current-time", "captions", "pip", "airplay", "fullscreen"],
      })
  );
}
